<?php
/**
 * @file
 * Administration page callbacks for the social_tabs module
 */

/**
 * Form builder, Configure social_tabs
 * 
 * @ingroup
 * @see system_settings_form()
 */
function social_tabs_admin_settings() {
  global $base_url;
  $facebook_enabled = variable_get('social_tabs_facebook_enabled', NULL); 
  $twitter_enabled = variable_get('social_tabs_twitter_enabled', NULL);
 
  $form['social_tabs'] = array();

  // Facebook Group
  $form['social_tabs']['facebook_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Facebook Settings',
    '#weight' => 5,
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
  );
  $form['social_tabs']['facebook_settings']['facebook_settings_enabled'] = array(
    '#type'         => 'checkbox',
    '#title'        => 'Enable facebook feed',
    '#default_value'       => variable_get("social_tabs_facebook_enabled", 0), 
  );

  $form['social_tabs']['facebook_settings']['facebook_settings_appid'] = array(
    '#type'         => 'textfield',
    '#title'        => 'Application ID',
    '#default_value'      => variable_get('social_tabs_facebook_appid', ""), 
    '#description'        => "In order for the facebook feed to work, you must first create an application on the <a href='https://developers.facebook.com/apps'> Facebook Developers page </a>.
				 The only thing you will need to specify is the url that you will be using the application from ( i.e web.drupal.com )  in the web settings of the application.",
  );
  
  $form['social_tabs']['facebook_settings']['facebook_settings_appsecret'] = array(
    '#type'         => 'textfield',
    '#title'        => 'Application Secret',
    '#default_value'      => variable_get('social_tabs_facebook_appsecret', ""), 
    '#description'        => "In order for the facebook feed to work, you must first create an application on the <a href='https://developers.facebook.com/apps'> Facebook Developers page </a>.
				 The only thing you will need to specify is the url and domain that you will be using the application from ( i.e web.drupal.com )  in the web settings of the application.",
  );

  $form['social_tabs']['facebook_settings']['facebook_settings_token'] = array(
    '#type'     => 'textfield',
    '#title'    => 'Authentication Token',
    '#disabled'   => TRUE,
    '#id'     => 'clear_facebook_text',
    '#default_value'  => variable_get('social_tabs_facebook_authtoken', ""),  
    '#description'    => "This field will be auto populated after submitting the form. If you want to reauthorize facebook <a id='clear_facebook_link'> clear the field </a>", 
  );
  $form['social_tabs']['facebook_settings']['facebook_settings_user_group_id'] = array(
    '#type'    => 'textfield',
    '#title'   => 'User/Group ID',
    '#default_value' => variable_get('social_tabs_facebook_userid', ""),
    '#required'  => FALSE,
  );
  
  // Twitter Group
  $form['social_tabs']['twitter_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Twitter Settings',
    '#weight' => 5,
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
  );
  $form['social_tabs']['twitter_settings']['twitter_settings_enabled'] = array(
    '#type'          => 'checkbox',
    '#title'         => 'Enable twitter feed',
    '#default_value'   => variable_get("social_tabs_twitter_enabled", 0),

  );  
  $form['social_tabs']['twitter_settings']['twitter_settings_user_id'] = array(
    '#type'          => 'textfield',
    '#title'       => 'User ID',
    '#default_value' => variable_get('social_tabs_twitter_userid', ""),
    '#required'      => FALSE, 
  );

  $form['#submit'][] = 'social_tabs_admin_settings_submit';
  return system_settings_form($form);
}

/**
 * Form Validation 
 */

function social_tabs_admin_settings_validate($form, &$form_state) {
  if ($form_state['values']['facebook_settings_enabled']) {
    // Make sure the user also set app_id, app_secret and user/group id
    if (!$form_state['values']['facebook_settings_appid']) {
      form_set_error('facebook_settings_appid', t("Application ID must be set to enable Facebook"));
    } 
    if (!$form_state['values']['facebook_settings_appsecret']) {
      form_set_error('facebook_settings_appsecret', t("Application Secret must be set to enable Facebook"));
    }
    if (!$form_state['values']['facebook_settings_user_group_id']) {
      form_set_error('facebook_settings_user_group_id', t("User/Group ID must be set to enable Facebook"));
    }
  } 

 
  // Validate Twitter
  if ($form_state['values']['twitter_settings_enabled']) {
    if (!$form_state['values']['twitter_settings_user_id']) {
      form_set_error('twitteer_settings_user_id', t("The Username must be set to enable Twitter"));
    }
  }
}

/**
 * Form Submittion
 * Process the social tabs admin settings form submission
 */
function social_tabs_admin_settings_submit($form, &$form_state) {
  global $base_url;
  variable_set('social_tabs_facebook_enabled', $form_state['values']['facebook_settings_enabled']);
  variable_set('social_tabs_facebook_appid', $form_state['values']['facebook_settings_appid']);
  variable_set('social_tabs_facebook_appsecret', $form_state['values']['facebook_settings_appsecret']);
  variable_set('social_tabs_facebook_authtoken',  $form_state['values']['facebook_settings_token'] ? $form_state['values']['facebook_settings_token'] : "");
  variable_set('social_tabs_facebook_userid',  $form_state['values']['facebook_settings_user_group_id']);
  //If facebook enabled is set and token is not then setup the token
  if ($form_state['values']['facebook_settings_enabled'] && empty($form_state['values']['facebook_settings_token'])) {
  $form_state['redirect'] = "https://www.facebook.com/dialog/oauth?client_id={$form_state['values']['facebook_settings_appid']}&redirect_uri={$base_url}/social_tabs/facebook/register&scope=read_stream,offline_access"; 
  }
  variable_set('social_tabs_twitter_enabled', $form_state['values']['twitter_settings_enabled']);
  variable_set('social_tabs_twitter_userid', $form_state['values']['twitter_settings_user_id']);
}

/**
 * Register for facebook access
 */
function social_tabs_register_facebook($code="") {
  global $base_url;
  if (isset($_GET['code'])) {
        $code = $_GET['code']; } 
  // now we need to authorize the application
  $appid = variable_get('social_tabs_facebook_appid', "");
  $appsecret = variable_get('social_tabs_facebook_appsecret', ""); 
  $url = $base_url . "/social_tabs/facebook/register"; 
  if ($appid == "" || $appsecret == "") {
    watchdog("social_tabs", "Apparently appid or appsecret are emtpy -- $appid --- $appsecret ---");  
    drupal_goto($base_url . "/admin/config/services/facebook_pull"); 
  } 
     else {  
    watchdog("social_tabs", "OK appid and appsecret have a value");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/oauth/access_token?client_id={$appid}&redirect_uri={$url}&client_secret={$appsecret}&code={$code}");
    $token_whole = curl_exec($ch);
    $token = explode("=", $token_whole);
    watchdog("social_tabs", "Setting authtoken to {$token[1]} --- ");
    variable_set('social_tabs_facebook_authtoken', $token[1]);     
    drupal_goto($base_url . "/admin/config/services/facebook-pull"); 
  }
}



