<div class="social-tabs">
  <div id="tabs">
    <ul>
      <?php if(isset($facebook_feed)): ?>
        <?php if($facebook_feed != ""): ?>
        <li>
	  <a href="#facebook">
	    <img src="<?php print base_path(); print $module_path ?>/images/facebook1.png" />
          </a>
	</li> 
	<?php endif; ?>
      <?php endif; ?>
      <?php if(isset($twitter_feed)): ?>
        <li>
	  <a href="#twitter">
	    <img src="<?php print base_path(); print $module_path ?>/images/twitter.png" />
          </a>
	</li> 
      <?php endif; ?>
    </ul>

    <?php if(isset($facebook_feed)): ?>
      <?php if($facebook_feed != ""): ?>
        <div id="facebook">
          <?php print $facebook_feed; ?>
        </div>
      <?php endif; ?>
    <?php endif; ?>

    <?php if(isset($twitter_feed)): ?>
    <div id="twitter">
      <?php print $twitter_feed; ?> 
    </div>
    <?php endif; ?>
  </div>
</div>
